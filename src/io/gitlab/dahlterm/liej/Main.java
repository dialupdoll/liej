package io.gitlab.dahlterm.liej;

public class Main {

public static void main(String[] args) {
	// write your code here
	io.gitlab.dahlterm.liej.language.TestingSuite.runtests();

	String lexerTest = "name . integer number;.number = 10;\n" +
						 "\n" +
						 "print .number;\n" +
						 "\n" +
						 "name ., variant;\n" +
						 "name .variant string nestedExample;\n" +
						 "\n" +
						 ".variant.nestedExample = \"hello, world\";\n" +
						 "\n" +
						 "print .variant.nestedExample;\n" +
						 "\n" +
						 "name . list list2;\n" +
						 "\n" +
						 ".list2 = :{\n" +
						 "\tname ., nestedExample;\n" +
						 "\t.nestedExample = \"hello, world\";\n" +
						 "\tname ., nestedSecondary;\n" +
						 "\tnestedSecondary = 5;\n" +
						 "\treturn .;\n" +
						 "};\n" +
						 "\n" +
						 "print .list2.nestedExample;\n" +
						 "print .list2.nestedSecondary;\n" +
						 "\n" +
						 ".list2 = (\"hello 2 world!\", 6);\n" +
						 "\n" +
						 "print .list2.nestedExample;\n" +
						 "print .list2.nestedSecondary;\n" +
						 "\n" +
						 "enable meta;\n" +
						 "print .;";

	Parser.LexerFunctions.LexerResult currentData = Parser.LexerFunctions.nextStatement(lexerTest);
	while (currentData != null) {
		System.out.println("Line: " + '"' + currentData.result + '"' + "");

		if (currentData != null) {

			System.out.print("Tokens: {");
			Parser.LexerFunctions.LexerResult tokenIterator =
			  Parser.LexerFunctions.nextToken(currentData.result);
			while (tokenIterator != null & !currentData.result.isEmpty()) {
				System.out.print('"' + tokenIterator.result + '"');
				tokenIterator =
				  Parser.LexerFunctions.nextToken(tokenIterator.leftover);

				if (tokenIterator != null && tokenIterator.result != null) {
					System.out.print(", ");
				}
			}
			System.out.println("}");
			System.out.println();
		}
		currentData = Parser.LexerFunctions.nextStatement(currentData.leftover);
	}

}
}
