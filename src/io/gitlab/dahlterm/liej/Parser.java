package io.gitlab.dahlterm.liej;

import io.gitlab.dahlterm.liej.language.types.LIE_Object;
import io.gitlab.dahlterm.liej.language.types.LIE_Sequence;
import io.gitlab.dahlterm.liej.language.types.LIE_Type;
import io.gitlab.dahlterm.liej.language.types.LIE_TypeValidator;

public class Parser {
public class LexerFunctions {


	public static char firstCharacter(String input) {
		return input.charAt(0);
	}

	public static char lastCharacter(String input) {
		return input.charAt(input.length() - 1);
	}

	public static String trimmableCharacters() {
		return " \t" + System.lineSeparator();
	}

	public static int firstInstanceOfAnycharacter(String input, String match) {

		char[] charArray = input.toCharArray();
		char[] matchArray = match.toCharArray();
		int firstCharacterMatch = -1;
		for (int iterator = 0; iterator < charArray.length; iterator++) {
			char subjectCharacter = charArray[iterator];
			for (char matchCharacter : matchArray) {
				if (subjectCharacter == matchCharacter) {
					if (firstCharacterMatch == -1) {
						firstCharacterMatch = iterator;
						return firstCharacterMatch;
					}
				}
			}
		}
		return firstCharacterMatch;
	}

	public static String trim(String input) {
		String result = input.trim();
		String trimKey = trimmableCharacters();
		for (char c : trimKey.toCharArray()) {
			if (result.length() == 0) {
				return "";
			}
			if (lastCharacter(result) == c) {
				while (lastCharacter(result) == c) {
					if (result.length() == 0) {
						return "";
					}
					result = result.substring(0, result.length() - 1);
				}
			}
			if (firstCharacter(result) == c) {
				while (firstCharacter(result) == c) {
					if (result.length() == 0) {
						return "";
					}
					result = result.substring(1);
				}
			}
			result = input.trim();
		}
		return result;
	}

	public static class LexerResult {

		String result;
		String leftover;
	}

	public static final String nonWhitespaceDelimitedTokens = "{};&:.";


	public static LexerResult nextToken(String input) {
		input = trim(input);
		LexerResult result = new LexerResult();

		int firstTrimmable = firstInstanceOfAnycharacter(input,
		  trimmableCharacters());
		int firstNonDelimitedToken =
		  firstInstanceOfAnycharacter(input, nonWhitespaceDelimitedTokens);


		if (firstNonDelimitedToken == -1 & firstTrimmable == -1)
			return null;
		if (firstNonDelimitedToken != -1 & firstTrimmable != -1) {
			if (firstNonDelimitedToken < firstTrimmable) {
				firstTrimmable = -1;
			} else if (firstNonDelimitedToken > firstTrimmable) {
				firstNonDelimitedToken = -1;
			}
		}
		if (firstNonDelimitedToken != -1) {
			if (firstNonDelimitedToken == 0) {
				//System.out.println("QAQ");
				result.result = String.valueOf(input.charAt(0));
				result.leftover = input.substring(1);
				result.leftover = trim(result.leftover);
				result.result = trim(result.result);
				return result;

			}
			result.result = input.substring(0, firstNonDelimitedToken);
			result.leftover = input.substring(firstNonDelimitedToken);
			result.leftover = trim(result.leftover);
			result.result = trim(result.result);
			return result;
		}
		if (firstTrimmable != -1) {
			result.leftover = input.substring(firstTrimmable + 1);
			result.result = input.substring(0, firstTrimmable);
			result.leftover = trim(result.leftover);
			result.result = trim(result.result);
			return result;
		}
		return null;
	}

	public static LexerResult nextStatement(String input) {
		LexerResult result = new LexerResult();
		int next = input.indexOf(";");
		if (next != -1) {
			result.result = input.substring(0, next + 1);
			result.result.trim();
			result.leftover = input.substring(next + 1);

			result.result = trim(result.result);
			return result;
		} else {
			return null;
		}
	}
}

public class RunInstance {
	private final LIE_Sequence scope = null;
}

public static boolean castingStrict = false;

public static boolean verifyType(String typeNameString, LIE_Object value, LIE_Sequence memory) {

	LIE_Type typeJType = LIE_Type.fundamentalTypeFromString(typeNameString);

	LIE_Type fundamentalType = value.fundamentalType();

	return typeJType.equals(fundamentalType);

	//TODO: add checking against memory for runtime types
}

public static LIE_TypeValidator lookupTypeByName(LIE_Sequence scope, String name) {
	LIE_Type builtins = LIE_Type.fundamentalTypeFromString(name);
	if (builtins != LIE_Type.LIE_NULL && builtins != LIE_Type.LIE_TYPE_ERROR) {

	}

	return null;
}

public static LIE_Sequence parse(String expression) {
	return null;
}
}
