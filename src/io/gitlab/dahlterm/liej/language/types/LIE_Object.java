package io.gitlab.dahlterm.liej.language.types;

public class LIE_Object {
public Object value;

public LIE_Type fundamentalType() {
	if (value == null) {
		return LIE_Type.LIE_NULL;
	}

	if (value instanceof LIE_Sequence) {
		return LIE_Type.LIE_SEQUENCE;
	} else if (value instanceof Byte) {
		return LIE_Type.LIE_BYTE;
	} else if (value instanceof Integer) {
		return LIE_Type.LIE_INT;
	} else if (value instanceof Boolean) {
		return LIE_Type.LIE_BOOLEAN;
	} else if (value instanceof Double) {
		return LIE_Type.LIE_DOUBLE;
	} else if (value instanceof Float) {
		return LIE_Type.LIE_FLOAT;
	} else if (value instanceof Long) {
		return LIE_Type.LIE_LONG;
	} else if (value instanceof String) {
		return LIE_Type.LIE_STRING;
	} else if (value instanceof Character) {
		return LIE_Type.LIE_CHARACTER;
	} else if (value instanceof LIE_Type) {
		return LIE_Type.LIE_TYPE;
	}

	return LIE_Type.LIE_TYPE_ERROR;
}

public boolean isNumberType() {
	LIE_Type fund = fundamentalType();
	switch (fund) {
		case LIE_BYTE:
			return true;
		case LIE_INT:
			return true;
		case LIE_LONG:
			return true;
		case LIE_CHARACTER:
			return false;
		case LIE_STRING:
			return false;
		case LIE_FLOAT:
			return true;
		case LIE_TYPE:
			return false;
		case LIE_SEQUENCE:
			return false;
		case LIE_BOOLEAN:
			return false;
		case LIE_DOUBLE:
			return false;
		case LIE_NULL:
			return false;
		default:
			System.err.println("Should never occur: " + fund + " is not a fundamental type");
			return false;
	}

}

public LIE_Object convert(LIE_Type targetType) {

	if (targetType.equals(fundamentalType())) {
		return this;
	}

	if (targetType.equals(LIE_Type.LIE_BYTE)) {

		if (fundamentalType().equals(LIE_Type.LIE_INT)) {
			int i = (int) value;
			return new LIE_Object((byte) i);
		} else if (fundamentalType().equals(LIE_Type.LIE_BOOLEAN)) {
			boolean b = (boolean) value;
			if (b) {
				return new LIE_Object((byte) 1);
			} else {
				return new LIE_Object((byte) 0);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_DOUBLE)) {
			double d = (double) value;
			return new LIE_Object((byte) d);
		} else if (fundamentalType().equals(LIE_Type.LIE_LONG)) {
			long l = (long) value;
			return new LIE_Object((byte) l);
		} else if (fundamentalType().equals(LIE_Type.LIE_SHORT)) {
			short s = (short) value;
			return new LIE_Object((byte) s);
		} else if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
			float f = (float) value;
			return new LIE_Object((byte) f);
		} else if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String s = (String) value;
			try {
				return new LIE_Object((byte) Integer.parseInt(s));
			} catch (NumberFormatException nfe) {
				return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_CHARACTER)) {
			char c = (char) value;
			return new LIE_Object((byte) c);
		} else if (fundamentalType().equals(LIE_Type.LIE_TYPE)) {
			new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}

	} else if (targetType.equals(LIE_Type.LIE_INT)) {

		if (fundamentalType().equals(LIE_Type.LIE_BYTE)) {
			byte b = (byte) value;
			return new LIE_Object(b);
		} else if (fundamentalType().equals(LIE_Type.LIE_BOOLEAN)) {
			boolean b = (boolean) value;
			if (b) {
				return new LIE_Object(1);
			} else {
				return new LIE_Object(0);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_DOUBLE)) {
			double d = (double) value;
			return new LIE_Object((int) d);
		} else if (fundamentalType().equals(LIE_Type.LIE_LONG)) {
			long l = (long) value;
			return new LIE_Object((int) l);
		} else if (fundamentalType().equals(LIE_Type.LIE_SHORT)) {
			short s = (short) value;
			return new LIE_Object(Short.toUnsignedInt(s));
		} else if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
			float f = (float) value;
			return new LIE_Object(Float.floatToIntBits(f));
		} else if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String s = (String) value;
			try {
				return new LIE_Object(Integer.parseInt(s));
			} catch (NumberFormatException nfe) {
				return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_CHARACTER)) {
			char c = (char) value;
			return new LIE_Object(c);
		} else if (fundamentalType().equals(LIE_Type.LIE_TYPE)) {
			new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}

	} else if (targetType.equals(LIE_Type.LIE_BOOLEAN)) {

		LIE_Object i = convert(LIE_Type.LIE_INT);
		if (!i.value.equals(LIE_Type.LIE_TYPE_ERROR)) {
			int val = (int) i.value;
			if (val != 0) {
				return new LIE_Object(true);
			} else {
				return new LIE_Object(false);
			}
		}
		if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String str = (String) value;
			if (str.equalsIgnoreCase("TRUE")) {
				return new LIE_Object(true);
			} else if (str.equalsIgnoreCase("FALSE")) {
				return new LIE_Object(false);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_NULL)) {
			return new LIE_Object(false);
		}
		return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);

	} else if (targetType.equals(LIE_Type.LIE_DOUBLE)) {

		if (fundamentalType().equals(LIE_Type.LIE_BYTE)) {
			byte b = (byte) value;
			return new LIE_Object((double) b);
		} else if (fundamentalType().equals(LIE_Type.LIE_INT)) {
			int i = (int) value;
			return new LIE_Object((double) i);
		} else if (fundamentalType().equals(LIE_Type.LIE_BOOLEAN)) {
			boolean b = (boolean) value;
			if (b) {
				return new LIE_Object(1);
			} else {
				return new LIE_Object(0);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_LONG)) {
			long l = (long) value;
			return new LIE_Object((int) l);
		} else if (fundamentalType().equals(LIE_Type.LIE_SHORT)) {
			short s = (short) value;
			return new LIE_Object(Short.toUnsignedInt(s));
		} else if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
			float f = (float) value;
			return new LIE_Object((double) f);
		} else if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String s = (String) value;
			try {
				return new LIE_Object(Integer.parseInt(s));
			} catch (NumberFormatException nfe) {
				return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_CHARACTER)) {
			char c = (char) value;
			return new LIE_Object(c);
		} else if (fundamentalType().equals(LIE_Type.LIE_TYPE)) {
			new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}

	} else if (targetType.equals(LIE_Type.LIE_LONG)) {

		if (fundamentalType().equals(LIE_Type.LIE_BYTE)) {
			byte b = (byte) value;
			return new LIE_Object(b);
		} else if (fundamentalType().equals(LIE_Type.LIE_INT)) {
			int i = (int) value;
			return new LIE_Object((double) i);
		} else if (fundamentalType().equals(LIE_Type.LIE_BOOLEAN)) {
			boolean b = (boolean) value;
			if (b) {
				return new LIE_Object(1);
			} else {
				return new LIE_Object(0);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_DOUBLE)) {
			double d = (double) value;
			return new LIE_Object(d);
		} else if (fundamentalType().equals(LIE_Type.LIE_SHORT)) {
			short s = (short) value;
			return new LIE_Object(Short.toUnsignedInt(s));
		} else if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
			float f = (float) value;
			return new LIE_Object(Float.floatToIntBits(f));
		} else if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String s = (String) value;
			try {
				return new LIE_Object(Integer.parseInt(s));
			} catch (NumberFormatException nfe) {
				return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_CHARACTER)) {
			char c = (char) value;
			return new LIE_Object(c);
		} else if (fundamentalType().equals(LIE_Type.LIE_TYPE)) {
			new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}

	} else if (targetType.equals(LIE_Type.LIE_SHORT)) {

		if (fundamentalType().equals(LIE_Type.LIE_BYTE)) {
			Byte b = (Byte) value;
			return new LIE_Object((short) b);
		} else if (fundamentalType().equals(LIE_Type.LIE_INT)) {
			int i = (int) value;
			return new LIE_Object((short) i);
		} else if (fundamentalType().equals(LIE_Type.LIE_BOOLEAN)) {
			boolean b = (boolean) value;
			if (b) {
				return new LIE_Object((short) 1);
			} else {
				return new LIE_Object((short) 0);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_DOUBLE)) {
			double d = (double) value;
			return new LIE_Object((short) d);
		} else if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
			float f = (float) value;
			return new LIE_Object((short) f);
		} else if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
			String s = (String) value;
			try {
				return new LIE_Object(Short.parseShort(s));
			} catch (NumberFormatException nfe) {
				return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
			}
		} else if (fundamentalType().equals(LIE_Type.LIE_CHARACTER)) {
			char c = (char) value;
			return new LIE_Object((short) c);
		} else if (fundamentalType().equals(LIE_Type.LIE_TYPE)) {
			new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}

	} else if (targetType.equals(LIE_Type.LIE_FLOAT)) {
		if (fundamentalType().equals(LIE_Type.LIE_INT)) {
			int i = (int) value;
			return new LIE_Object((float) i);
		}
	}
	//JBYTE, JINT, JBOOLEAN, JDOUBLE, JLONG, JSHORT, JFLOAT, JSTRING, JCHARACTER, JTYPE, JNULL, TYPE_ERROR;
		/*
		 * else if(fundamentalType().equals(JType.)) {
				
			}
		 */
	System.err.println("Not yet implemented conversion, or unable to figure out how to convert it!");
	System.err.println("Type converting from: " + fundamentalType().toString());
	System.err.println("Type converting to: " + targetType);
	return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
}

/*
 * initializers to convert from native types
 */

public LIE_Object(Object data) {
	value = data;
}

public LIE_Object(int data) {
	value = new Integer(data);
}

public LIE_Object(float data) {
	value = new Float(data);
}


public byte toByte() {
	return (byte) convert(LIE_Type.LIE_BYTE).value;
}

public byte asByte() {
	if (fundamentalType().equals(LIE_Type.LIE_BYTE)) {
		return (byte) value;
	} else {
		if (isNumberType()) {
			return toByte();
		}
		System.err.println("not able to be used as an byte!");
		return -1;
	}
}

public int toInteger() {
	return (int) convert(LIE_Type.LIE_INT).value;
}

public int asInteger() {
	if (fundamentalType().equals(LIE_Type.LIE_INT)) {
		return (int) value;
	} else {
		if (isNumberType()) {
			return toInteger();
		}
		System.err.println("not able to be used as an integer!");
		return -1;
	}
}

public short toShort() {
	return (short) convert(LIE_Type.LIE_SHORT).value;
}

public short asShort() {
	if (fundamentalType().equals(LIE_Type.LIE_SHORT)) {
		return (short) value;
	} else {
		if (isNumberType()) {
			return toShort();
		}
		System.err.println("not able to be used as an short!");
		return -1;
	}
}

public long toLong() {
	System.err.println("to long conversion unimplemented!");
	return -1;
}

public long asLong() {
	if (fundamentalType().equals(LIE_Type.LIE_LONG)) {
		return (long) value;
	} else {
		if (isNumberType()) {
			return toLong();
		}
		System.err.println("not able to be used as an long!");
		return -1;
	}
}

public float toFloat() {
	return (float) convert(LIE_Type.LIE_FLOAT).value;
}

public float asFloat() {
	if (fundamentalType().equals(LIE_Type.LIE_FLOAT)) {
		return (float) value;
	} else {
		if (isNumberType()) {
			return toFloat();
		}
		System.err.println("not able to be used as a float!");
		return -1;
	}
}

public double toDouble() {
	return (double) convert(LIE_Type.LIE_DOUBLE).value;
}

public double asDouble() {
	if (fundamentalType().equals(LIE_Type.LIE_DOUBLE)) {
		return (double) value;
	} else {
		if (isNumberType()) {
			return toDouble();
		}
		System.err.println("not able to be used as an integer!");
		return -1;
	}
}

public LIE_Sequence asSequence() {
	if (fundamentalType().equals(LIE_Type.LIE_SEQUENCE)) {
		return (LIE_Sequence) value;
	}
	System.err.println("not able to be used as an sequence!");
	return null;
}

public String asString() {
	if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
		return (String) value;
	} else {
		return asSimpleString();
	}
	// TODO: 7/8/22 ADD STRINGIFICATION PRETTY-PRINT-BY-DEFAULT support
}

public String asSimpleString() {
	if (fundamentalType().equals(LIE_Type.LIE_STRING)) {
		return "\"" + value + "\"";
	}
	if (fundamentalType().equals(LIE_Type.LIE_SEQUENCE)) {
		int interator = 1;
		String result = "{";
		while (interator <= asSequence().length().asInteger()) {
			LIE_Object o_iterator = asSequence().get(new LIE_Object(interator));
			if (o_iterator.fundamentalType().equals(LIE_Type.LIE_STRING)) {
				result += "\"" + o_iterator.value + "\"";
			} else {
				result += o_iterator.asSimpleString();
			}
			interator++;
			if (interator <= asSequence().length().asInteger()) {
				result += ", ";
			}
		}
		result += "}";
		return result;
	}
	if (isNumberType()) {
		return "" + value;
	}
	//return toString();
	return convert(LIE_Type.LIE_STRING).asString();
}

public Number asNumber() {
	switch (fundamentalType()) {
		case LIE_BYTE:
			return asByte();
		case LIE_INT:
			return asInteger();
		case LIE_SHORT:
			return asShort();
		case LIE_LONG:
			return asLong();
		case LIE_FLOAT:
			return asFloat();
		case LIE_DOUBLE:
			return asDouble();
		case RUNTIME:
			System.err.println("How the hell..?");
		case LIE_SEQUENCE:
		case LIE_STRING:
		case LIE_CHARACTER:
		case LIE_NULL:
		case LIE_TYPE:
		case LIE_TYPE_ERROR:
		case LIE_BOOLEAN:
		default:
			System.err.println("Can't convert to number type!");
			return null;
	}
}
}
