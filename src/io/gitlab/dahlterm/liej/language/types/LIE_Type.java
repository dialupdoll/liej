package io.gitlab.dahlterm.liej.language.types;

import io.gitlab.dahlterm.liej.Parser;

/*
 * 
switch() {
	case LIE_BOOLEAN:
	case LIE_BYTE:
	case LIE_INT:
	case LIE_SHORT:
	case LIE_LONG:
	case LIE_FLOAT:
	case LIE_DOUBLE:
	case LIE_STRING:
	case LIE_CHARACTER:
	case LIE_NULL:
	case RUNTIME:
	case LIE_SEQUENCE:
	case EL_TYPE:
	case EL_TYPE_ERROR:
	default:
}

 */
public enum LIE_Type implements LIE_TypeValidator {
	RUNTIME, LIE_SEQUENCE, LIE_BYTE, LIE_INT, LIE_BOOLEAN, LIE_DOUBLE, LIE_LONG, LIE_SHORT, LIE_FLOAT, LIE_STRING, LIE_CHARACTER, LIE_NULL, LIE_TYPE, LIE_TYPE_ERROR;
	
	public static LIE_Type fundamentalTypeFromString(String name) {
		
		name = name.trim();
		name = name.toLowerCase();
				
		if(name.equals("sequence")) {
			return LIE_SEQUENCE;
		} else if(name.equals("byte")) {
			return LIE_BYTE;
		} else if(name.equals("int")) {
			return LIE_INT;
		} else if(name.equals("boolean")) {
			return LIE_BOOLEAN;
		} else if(name.equals("double")) {
			return LIE_DOUBLE;
		} else if(name.equals("long")) {
			return LIE_LONG;
		} else if(name.equals("string")) {
			return LIE_STRING;
		} else if(name.equals("character")) {
			return LIE_CHARACTER;
		} else if(name.equals("type")) {
			return LIE_TYPE;
		} else if(name.equals("null")) {
			return LIE_NULL;
		} else if(name.equals("")) {
			return LIE_NULL;
		}
		
		return LIE_TYPE_ERROR;
	}
	public static String toString(LIE_Type type) {
		switch(type) {
		  case LIE_SEQUENCE:
			  return "sequence";
		  case LIE_BYTE:
			  return "byte";
		  case LIE_INT:
			  return "int";
		  case LIE_BOOLEAN:
			  return "boolean";
		  case LIE_DOUBLE:
			  return "double";
		  case LIE_LONG:
			  return "long";
		  case LIE_STRING:
			  return "string";
		  case LIE_CHARACTER:
			  return "character";
		  case LIE_TYPE:
			  return "type";
		  case LIE_NULL:
			  return "null";
		  case LIE_TYPE_ERROR:
		  default:
			  return "type error";
		}

	}
	@Override
	public boolean isValidType(LIE_Object value, LIE_Sequence scope) {
		if (value == null) return false;
		
		if (value.fundamentalType() == LIE_Type.LIE_TYPE_ERROR) return false;
		
		if(Parser.castingStrict) {
			
			if(value.fundamentalType() != this) {
				return false;
			}
		} else {
			LIE_Object test = value.convert(this);
			if(test.fundamentalType() == LIE_Type.LIE_TYPE) {
				if ((LIE_Type)test.value != LIE_Type.LIE_TYPE_ERROR) { // cannot convert if returns type error
					return false;
				}
			}
		}
		
		
		return true;
	}
}
