package io.gitlab.dahlterm.liej.language.types;

public interface LIE_TypeValidator {
	public boolean isValidType(LIE_Object value, LIE_Sequence scope);
}
