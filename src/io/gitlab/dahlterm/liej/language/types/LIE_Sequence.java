package io.gitlab.dahlterm.liej.language.types;

import java.util.ArrayList;

public class LIE_Sequence {
	private ArrayList<LIE_Object> data;
	
	public LIE_Object get(LIE_Object key) {
		//names will be checked for in the reflective zone if they're strings, integers are checked normally
		LIE_Object index = key.convert(LIE_Type.LIE_INT);
		int intdex = (int)index.value;
		try {
			if(index.fundamentalType().equals(LIE_Type.LIE_INT)) {
				return data.get(intdex);
			}
		} catch (IndexOutOfBoundsException ioobe) {
			ioobe.printStackTrace();
		}
		if(key.fundamentalType().equals(LIE_Type.LIE_STRING)) {
			///we need to do a lookup.
			System.err.println("lookups not implemented yet!");
		}
		return null;
	}
	
	public LIE_Object set(LIE_Object key, LIE_Object value) {
		if(key.fundamentalType() == LIE_Type.LIE_STRING) {
			// search reflective zone, type-error if not found, with more information to console
		} else if (key.fundamentalType() == LIE_Type.LIE_SEQUENCE) {
			// we now want to set each address/key in the sequence to the value selected. this does not account for swizzling, as it is sometimes called - it sets each index to the entirety of the right-side value
			// for now, leaving this unimplemented.
			return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		} else if(key.isNumberType() ) {
			switch(key.fundamentalType()) {
				case LIE_INT:
					int intdex = (int)key.value;
					try {
						data.set(intdex, value);
					} catch (IndexOutOfBoundsException ioobe) {
						while(intdex >= data.size()) {
							data.add(null);
						}
						data.set(intdex, value);
					}
					return value;
				case LIE_SHORT:
				case LIE_CHARACTER:
				case LIE_BOOLEAN:
				case LIE_BYTE:
				case LIE_DOUBLE:
				case LIE_FLOAT:
				case LIE_LONG:
					// these can all be converted to an integer value, and we will presume that is the intent.
					LIE_Object index = key.convert(LIE_Type.LIE_INT);
					//data.set((int)index.value, element)
				case LIE_STRING:
					// how the FUCK did you get here?
					// see if can treat string as a valid number parsed from
					// string, if not type error
					return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
				case RUNTIME:
					// not sure how to handle this yet - it shouldn't ever occur. but also, in general, runtime types need more thought.
					break;
				default:
					break;
			}
		} else {
		}
		// how the FUCK did you get here?
		return null;
	}
	
	public LIE_Sequence() {
		data = new ArrayList<LIE_Object> ();
		data.add(0, null);	// this initializes the reflective zone as being a null value
	}
	
	public LIE_Object length() {
		return new LIE_Object(data.size() - 1); //remove the data/reflective zone index
	}
	
}
 // if + operator then concatenate to appropriate side