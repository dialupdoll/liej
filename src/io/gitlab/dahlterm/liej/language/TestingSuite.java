package io.gitlab.dahlterm.liej.language;

import io.gitlab.dahlterm.liej.language.runtime.BuiltIns;
import io.gitlab.dahlterm.liej.language.types.LIE_Object;
import io.gitlab.dahlterm.liej.language.types.LIE_Sequence;

/****************************************************************************************************
 *                                                                                                  *
 *   LLLLLLLLLLL                  IIIIIIIIII     EEEEEEEEEEEEEEEEEEEEEE               JJJJJJJJJJJ   *
 *   L:::::::::L                  I::::::::I     E::::::::::::::::::::E               J:::::::::J   *
 *   L:::::::::L                  I::::::::I     E::::::::::::::::::::E               J:::::::::J   *
 *   LL:::::::LL                  II::::::II     EE::::::EEEEEEEEE::::E               JJ:::::::JJ   *
 *     L:::::L                      I::::I         E:::::E       EEEEEE                 J:::::J     *
 *	   L:::::L                      I::::I         E:::::E                              J:::::J     *
 *	   L:::::L                      I::::I         E::::::EEEEEEEEEE                    J:::::J     *
 *	   L:::::L                      I::::I         E:::::::::::::::E                    J:::::j     *
 *	   L:::::L                      I::::I         E:::::::::::::::E                    J:::::J     *
 *	   L:::::L                      I::::I         E::::::EEEEEEEEEE        JJJJJJJ     J:::::J     *
 *	   L:::::L                      I::::I         E:::::E                  J:::::J     J:::::J     *
 *	   L:::::L         LLLLLL       I::::I         E:::::E       EEEEEE     J::::::J   J::::::J     *
 *	 LL:::::::LLLLLLLLL:::::L     II::::::II     EE::::::EEEEEEEE:::::E     J:::::::JJJ:::::::J     *
 *	 L::::::::::::::::::::::L     I::::::::I     E::::::::::::::::::::E      JJ:::::::::::::JJ      *
 *	 L::::::::::::::::::::::L     I::::::::I     E::::::::::::::::::::E        JJ:::::::::JJ        *
 *	 LLLLLLLLLLLLLLLLLLLLLLLL     IIIIIIIIII     EEEEEEEEEEEEEEEEEEEEEE          JJJJJJJJJ          *
 *                                                                                                  *
 ****************************************************************************************************
 *		Lisp Inspired Euphoria on JVM
 *		(pronounced Liege)
 */

public class TestingSuite {

public static void runtests() {
	System.out.println();
	System.out.println("LIEJ testing");
	System.out.println();
	System.out.println("Addition test");
	System.out.println();
	int x = 0;

	LIE_Object number = new LIE_Object(12);
	LIE_Object number2 = new LIE_Object(12.7f);
	LIE_Object sequence = new LIE_Object(new LIE_Sequence());
	sequence.asSequence().set(new LIE_Object(1), number);
	sequence.asSequence().set(new LIE_Object(2), number2);
	LIE_Object added = BuiltIns.additionOperator.executeCode(sequence, null);
	System.out.println(added.value.toString());
	System.out.println();
	System.out.println(added.fundamentalType().toString());

	System.out.println();
	System.out.println("Sequence print test");
	System.out.println();

	LIE_Object sequencePrintTest = new LIE_Object(new LIE_Sequence());
	sequencePrintTest.asSequence().set(new LIE_Object(1), new LIE_Object(
	  "test"));
	sequencePrintTest.asSequence().set(new LIE_Object(2), new LIE_Object(
	  1.688f));
	sequencePrintTest.asSequence().set(new LIE_Object(3),
	  sequence);
	System.out.println(sequencePrintTest.asSimpleString());
	System.out.println();
	System.out.println();
	System.out.println();
	System.out.println();
}

}
