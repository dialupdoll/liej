# Tokens

* Tokens are either an operator, a function name, a value, or a delimiter.

---

# Operators

* Binary operators: = + - * / . : as else
* Unary (function symbols): & ;

---

# Delimiters

* { and }: code block
* " and ": string
* ( and , and ): list, explicit rather than implicit, for better nesting capabilities, optional
  in many cases

---

# Builtin Functions

* name
* print
* if
* else
* return

---

# NonSpaceDelimited Tokens

* {
* }
* ,
* =

---

# Examples in code form

#### Name operator, and lists/scopes

```
name . integer number;

.number = 10;

print .number;

name ., variant;
name .variant string nestedExample;

.variant.nestedExample = "hello, world";

print .variant.nestedExample;

name . list list2;

.list2 = :{
	name ., nestedExample;
	.nestedExample = "hello, world";
	name ., nestedSecondary;
	nestedSecondary = 5;
	return .;
};

print .list2.nestedExample;
print .list2.nestedSecondary;

.list2 = ("hello 2 world!", 6);

print .list2.nestedExample;
print .list2.nestedSecondary;

enable meta;
print .;

```

should output

```
10
hello, world
hello, world
5
hello 2 world!
6
(
  (
    (1, "number", integer),
    (2, "variant"),
    (3, "list2", list)
  ), (
    10,
    (
      (
        (1, "nestedExample", string)
      ),
      (
        "hello, world"
      )
    ),
    (
      (
        (1, "nestedExample"),
        (2, "nestedSecondary")
      ),
      (
        "hello 2 world!",
        6
      )
    ),
  )
)
```