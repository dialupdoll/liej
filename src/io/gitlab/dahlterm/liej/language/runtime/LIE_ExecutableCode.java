package io.gitlab.dahlterm.liej.language.runtime;

import io.gitlab.dahlterm.liej.language.types.LIE_Object;
import io.gitlab.dahlterm.liej.language.types.LIE_Sequence;

public interface LIE_ExecutableCode {
	public LIE_Object executeCode(LIE_Object arguments, LIE_Sequence scope);
}
