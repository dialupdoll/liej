package io.gitlab.dahlterm.liej.language.runtime;

import io.gitlab.dahlterm.liej.language.types.LIE_Object;
import io.gitlab.dahlterm.liej.language.types.LIE_Sequence;
import io.gitlab.dahlterm.liej.language.types.LIE_Type;
import io.gitlab.dahlterm.liej.language.types.LIE_TypeValidator;

/****************************************************************************************************
 *                                                                                                  *
 *   LLLLLLLLLLL                  IIIIIIIIII     EEEEEEEEEEEEEEEEEEEEEE               JJJJJJJJJJJ   *
 *   L:::::::::L                  I::::::::I     E::::::::::::::::::::E               J:::::::::J   *
 *   L:::::::::L                  I::::::::I     E::::::::::::::::::::E               J:::::::::J   *
 *   LL:::::::LL                  II::::::II     EE::::::EEEEEEEEE::::E               JJ:::::::JJ   *
 *     L:::::L                      I::::I         E:::::E       EEEEEE                 J:::::J     *
 *	   L:::::L                      I::::I         E:::::E                              J:::::J     *
 *	   L:::::L                      I::::I         E::::::EEEEEEEEEE                    J:::::J     *
 *	   L:::::L                      I::::I         E:::::::::::::::E                    J:::::j     *
 *	   L:::::L                      I::::I         E:::::::::::::::E                    J:::::J     *
 *	   L:::::L                      I::::I         E::::::EEEEEEEEEE        JJJJJJJ     J:::::J     *
 *	   L:::::L                      I::::I         E:::::E                  J:::::J     J:::::J     *
 *	   L:::::L         LLLLLL       I::::I         E:::::E       EEEEEE     J::::::J   J::::::J     *
 *	 LL:::::::LLLLLLLLL:::::L     II::::::II     EE::::::EEEEEEEE:::::E     J:::::::JJJ:::::::J     *
 *	 L::::::::::::::::::::::L     I::::::::I     E::::::::::::::::::::E      JJ:::::::::::::JJ      *
 *	 L::::::::::::::::::::::L     I::::::::I     E::::::::::::::::::::E        JJ:::::::::JJ        *
 *	 LLLLLLLLLLLLLLLLLLLLLLLL     IIIIIIIIII     EEEEEEEEEEEEEEEEEEEEEE          JJJJJJJJJ          *
 *                                                                                                  *
 ****************************************************************************************************
 *		Lisp Inspired Euphoria on JVM
 *		(pronounced Liege)
 */

public class BuiltIns {
public static final LIE_ExecutableCode additionOperator = new LIE_ExecutableCode() {

	public LIE_Object executeCode(LIE_Object arguments, LIE_Sequence scope) {
		if (arguments.fundamentalType() != LIE_Type.LIE_SEQUENCE) {
			System.err.println("Arguments given are not a sequence! Cannot add without two values");
			return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);
		}
		LIE_Sequence sArguments = (LIE_Sequence) arguments.value;

		LIE_Object oLeft = sArguments.get(new LIE_Object(1));
		LIE_Object oRight = sArguments.get(new LIE_Object(2));

		if (oLeft == null) {
			System.err.println("No left value! cannot add without two values");
			return null;
		}
		if (oRight == null) {
			System.err.println("No right value! cannot add without two values");
			return null;
		}
		LIE_Object result;
		/// early on we should check if we can just dodge it all by being the same type!
		if (oLeft.fundamentalType().equals(oRight.fundamentalType())) {
			switch (oLeft.fundamentalType()) {
				case LIE_TYPE_ERROR:
				case LIE_TYPE:
				case LIE_NULL:
				case LIE_BOOLEAN:
				case LIE_SEQUENCE:
				case LIE_STRING:
					System.err.println("Type " + oLeft.fundamentalType().toString() + " cannot do addition!");
					return null;
				case LIE_INT:

					result = new LIE_Object((int) oLeft.value + (int) oRight.value);
					return result;
				default:
					result = new LIE_Object(((Number) oLeft.value).doubleValue() + ((Number) oRight.value).doubleValue());
					return result;
			}
		}

		if (oLeft.fundamentalType() == LIE_Type.LIE_SEQUENCE) {
			LIE_Sequence sLeft = (LIE_Sequence) oLeft.value;
			System.err.println("sequence addition not implemented!");
			return null;
		}
		if (oLeft.fundamentalType().equals(LIE_Type.LIE_STRING)) {
			System.err.println("string addition not implemented!");
			return null;
		}
		if (oRight.fundamentalType().equals(LIE_Type.LIE_STRING)) {
			System.err.println("string addition not implemented!");
			return null;
		}
		LIE_Type mostPreciseType = null;
		// first we find the most precise type in the operation - we want to use that level of precision.
		mostPreciseType = oLeft.fundamentalType();
		switch (mostPreciseType) {
			case LIE_BYTE:
				mostPreciseType = oRight.fundamentalType();
				// every other type is more precise
				break;
			case LIE_INT:
				if (oRight.fundamentalType() != LIE_Type.LIE_BYTE) {
					mostPreciseType = oRight.fundamentalType();
				}
				break;
			case LIE_SHORT:
				if (oRight.fundamentalType() == LIE_Type.LIE_BYTE) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_INT) {
					break;
				}
				mostPreciseType = oRight.fundamentalType();
				break;
			case LIE_LONG:
				if (oRight.fundamentalType() == LIE_Type.LIE_BYTE) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_INT) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_LONG) {
					break;
				}
				mostPreciseType = oRight.fundamentalType();
				break;
			case LIE_FLOAT:
				if (oRight.fundamentalType() == LIE_Type.LIE_BYTE) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_INT) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_LONG) {
					break;
				}
				if (oRight.fundamentalType() == LIE_Type.LIE_FLOAT) {
					break;
				}
				mostPreciseType = oRight.fundamentalType();
				break;
			case LIE_DOUBLE:
				break; // the other one has to be less precise
			default:
				return null;
		}
		// now we have to convert both to the same, most precise type


		switch (mostPreciseType) {
			case LIE_INT:
				int i1 = oLeft.asInteger();
				int i2 = oRight.asInteger();
				return new LIE_Object(i1 + i2);
			case LIE_SHORT:
				short s1 = oLeft.asShort();
				short s2 = oRight.asShort();
				return new LIE_Object(s1 + s2);
			case LIE_LONG:
				long l1 = oLeft.asLong();
				long l2 = oRight.asLong();
				return new LIE_Object(l1 + l2);
			case LIE_FLOAT:
				float f1 = oLeft.asFloat();
				float f2 = oRight.asFloat();
				return new LIE_Object(f1 + f2);
			case LIE_DOUBLE:
				double d1 = oLeft.asDouble();
				double d2 = oRight.asDouble();
				return new LIE_Object(d1 + d2);
			default:
				return null;
		}

	}

};

public static final LIE_ExecutableCode nameFunction = new LIE_ExecutableCode() {    //name sequence variablename, type

	@Override
	public LIE_Object executeCode(LIE_Object arguments, LIE_Sequence scope) {
		if (arguments.fundamentalType() != LIE_Type.LIE_SEQUENCE)
			return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);


		return null;
	}

};

public static final LIE_ExecutableCode setOperator = new LIE_ExecutableCode() {

	@Override
	public LIE_Object executeCode(LIE_Object arguments, LIE_Sequence scope) {
		if (arguments.fundamentalType() != LIE_Type.LIE_SEQUENCE)
			return new LIE_Object(LIE_Type.LIE_TYPE_ERROR);


		return null;
	}

};

/*
 * ABOVE ↑ EXECUTABLE CODES
 *
 * BELOW ↓ TYPE VERIFIERS
 */

private class CodeStepPreprocess extends LIE_Sequence {

	public CodeStepPreprocess(String sourceCode) {

	}

	public void setSyntacticalSugaredAssignment(boolean status) {

	}

	public void setTarget(LIE_Object parameters) {

	}

	public void setFunction(String title) {

	}


	public boolean isValid() {
		return false;
	}
}

private class DelayedProcessingShard {
	String before, after;
}


public static final LIE_Sequence parseAsCode(LIE_Object codeblockSource) {
	String before = codeblockSource.asSimpleString();
	String after = null;
	LIE_Sequence tree = new LIE_Sequence();
	//while (!before.isBlank())
	if (before.trim().endsWith(
	  ")"
	)) {
		int splitout = before.lastIndexOf(
		  "("
		);
		//String nextParse = before.substring()
	}

	return null;
}

public static final LIE_ExecutableCode evaluateAsCode(LIE_Object codeblock) {

	String as_string = null;

	if (codeblock.fundamentalType() == LIE_Type.LIE_SEQUENCE) {
		/*
		 *	this means that the code we're evaluating is in
		 * static-definitions space, not within a function. we can get down
		 * to business anyway.
		 */
		as_string = codeblock.asSimpleString();

	} else if (codeblock.fundamentalType() == LIE_Type.LIE_STRING) {
		as_string = codeblock.toString();
	}
	return null;
}


public static final LIE_TypeValidator typeAnyCodeBlock =
  new LIE_TypeValidator() {
	  @Override
	  public boolean isValidType(LIE_Object value, LIE_Sequence scope) {


		  return false;
	  }
  };

}
